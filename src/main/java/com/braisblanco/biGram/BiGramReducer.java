package com.braisblanco.biGram;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.StringTokenizer;
import java.util.stream.Collectors;

import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.Reducer.Context;

public class BiGramReducer extends Reducer<IntWritable, Text, IntWritable, Text>{
	
	@Override
	public void reduce(IntWritable key, Iterable<Text> values, Context context) throws IOException, InterruptedException{
		HashMap<String, Integer> decadeMap = new HashMap<>();
				
		for(Text text : values) {
			StringTokenizer s = new StringTokenizer(text.toString(),"\t");
			String firstGram = s.nextToken();
			String secondGram = s.nextToken();
			String biGram = firstGram.concat(secondGram);
			int currentCount = Integer.parseInt(s.nextToken());
			
			if(decadeMap.containsKey(biGram)) {
				decadeMap.replace(biGram, decadeMap.get(biGram) + currentCount);
			}else {
				decadeMap.put(biGram, currentCount);
			}
			
		}
		
	    LinkedHashMap<String, Integer> result = decadeMap
	            .entrySet()
	            .stream()
	            .sorted(Collections.reverseOrder(Map.Entry.comparingByValue()))
	            .collect(
	            		Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue, (e1, e2) -> e2,
	                    LinkedHashMap::new));
	    
	    ArrayList<String> maxOneGrams = new ArrayList<String>();
	    int maxCount = result.values().iterator().next();
	    
	    for(java.util.Map.Entry<String, Integer> entry : result.entrySet()) {
	    	if(entry.getValue() == maxCount) {
	    		maxOneGrams.add(entry.getKey());
	    	}
	    }
	    
	    maxOneGrams.sort(String::compareToIgnoreCase);	    
	    
	    context.write(key, new Text(maxOneGrams.get(0) + "_" + maxCount));
	}

}
