package com.braisblanco.biGram;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.StringTokenizer;
import java.util.concurrent.TimeUnit;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.input.TextInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.mapreduce.lib.output.TextOutputFormat;

import com.braisblanco.oneGram.AGramm;
import com.braisblanco.oneGram.OneGramMapper;
import com.braisblanco.oneGram.OneGramReducer;
import com.fasterxml.jackson.databind.ObjectMapper;

public class BiGram {
	
	public static void main(String args[]) throws IOException, ClassNotFoundException, InterruptedException {
		
		LinkedHashMap<String,String> map = new LinkedHashMap<String, String>();
		
		Configuration conf = new Configuration();
		Job job1 = Job.getInstance(conf, "a-gramm");
		job1.setJarByClass(AGramm.class);
		job1.setMapperClass(OneGramMapper.class);
		//job.setCombinerClass(OneGramReducer.class);
		job1.setReducerClass(OneGramReducer.class);
		job1.setOutputKeyClass(IntWritable.class);
		job1.setOutputValueClass(Text.class);
		job1.setInputFormatClass(TextInputFormat.class);
        job1.setOutputFormatClass(TextOutputFormat.class);
		FileInputFormat.addInputPath(job1, new Path("/user/brais/input/googlebooks-spa-all-1gram-20120701-a"));
		//FileInputFormat.addInputPath(job1, new Path("prueba.txt"));
		FileOutputFormat.setOutputPath(job1, new Path("/user/brais/output_t1_p2_1"));
		
		System.out.println(job1.waitForCompletion(true) ? "BIEN" : "ERROR");
			
		Configuration conf2 = new Configuration();
		
		map = readOneGramOutput("/user/brais/output_t1_p2_1/part-r-00000", conf);
		conf2.set("oneGramMap", new ObjectMapper().writeValueAsString(map));

		Job job2 = Job.getInstance(conf2, "a-gramm");
	
		job2.setJarByClass(BiGram.class);
		job2.setMapperClass(BiGramMapper.class);
		job2.setReducerClass(BiGramReducer.class);
		job2.setOutputKeyClass(IntWritable.class);
		job2.setOutputValueClass(Text.class);
		job2.setInputFormatClass(TextInputFormat.class);
		job2.setOutputFormatClass(TextOutputFormat.class);
			
		FileInputFormat.addInputPath(job2, new Path("/user/brais/input/googlebooks-spa-all-2gram-20120701-a_"));
		//FileInputFormat.addInputPath(job2, new Path("prueba2.txt"));
		FileOutputFormat.setOutputPath(job2, new Path("/user/brais/output_t1_p2_2"));
		//set ouput name
		
		System.out.println(job2.waitForCompletion(true) ? "BIEN" : "ERROR");
			
	}
	
	public static LinkedHashMap<String, String> readOneGramOutput(String inFilePath, Configuration conf) throws IOException{
		LinkedHashMap<String, String> resultMap = new LinkedHashMap<String, String>();
		Path path = new Path(inFilePath);
		FileSystem fs = path.getFileSystem(conf);
		 
		BufferedReader reader = new BufferedReader(new InputStreamReader(fs.open(path)));
		String line = "";
		
		while( (line = reader.readLine()) != null) {
			StringTokenizer token = new StringTokenizer(line, "\t");
			String decade = token.nextToken();
			int decadInt = Integer.parseInt(decade);
			String oneGram = token.nextToken().split("_")[0];
				
			if((decadInt > 1899) && (decadInt <  2000)) {
				resultMap.put(decade, oneGram);
			}
		}
		
		reader.close();
		fs.close();
//		for(Entry<Integer, String> entry : resultMap.entrySet()) {
//			System.out.println("Decada: " + entry.getKey() + " oneGram: " + entry.getValue());
//		}
		
		return resultMap;
	}
	
}
