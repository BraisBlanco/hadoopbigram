package com.braisblanco.biGram;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.net.URL;
import java.util.LinkedHashMap;
import java.util.Map.Entry;
import java.util.StringTokenizer;

import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapred.jobcontrol.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;

import com.fasterxml.jackson.databind.ObjectMapper;


public class BiGramMapper extends Mapper<LongWritable, Text, IntWritable, Text>{
	
	private LinkedHashMap<String, String> oneGramMap;
	
	public LinkedHashMap<String, String> getOneGramMap() {
		return oneGramMap;
	}

	public void setOneGramMap(LinkedHashMap<String, String> oneGramMap) {
		this.oneGramMap = oneGramMap;
	}

	@Override
	public void setup(Context context) {

			try {
				System.out.println(context.getConfiguration().get("oneGramMap"));
				this.setOneGramMap(new ObjectMapper().readValue(context.getConfiguration().get("oneGramMap"), LinkedHashMap.class));			
			} catch (JsonParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (JsonMappingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			System.out.println("SUCCESS");

	}
	
	@Override
	public void map(LongWritable key, Text value, Context context) throws IOException, InterruptedException {
		LinkedHashMap<String, String> oneGramResult = this.getOneGramMap();
		StringTokenizer token = new StringTokenizer(value.toString(), "\t");
		String bigram = token.nextToken();
		String firstGram = bigram.split(" ")[0];
		String secondGram = bigram.split(" ")[1];
		String decade = token.nextToken();
		String count = token.nextToken();
			
		if(oneGramResult.containsKey(decade)) {
			if(oneGramResult.get(decade).equals(firstGram)) {
				context.write(new IntWritable(Integer.parseInt(decade)), new Text(firstGram.concat("\t").concat(secondGram).concat("\t").concat(count)));
			}
			context.getConfiguration().set("oneGramMap", new ObjectMapper().writeValueAsString(oneGramResult));
		}
	}
	
}
